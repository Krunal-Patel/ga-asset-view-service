﻿using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using TNM.NET45.Logging;
using System.Configuration;
using System.Threading;

namespace InsertGAView_Into_DB
{
    public class GADataFetcher
    {
        /// <summary>
        /// Get Number of Total Events breakdown by Event Lable,event Category,Event Action 
        /// </summary>
        /// <param name="GATrackerId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<GAEventData> Get_GAEventsData(int GATrackerId, DateTime startDate, DateTime endDate)
        {
            GaData gd = null;
            List<GAEventData> eventData = new List<GAEventData>();
            AnalyticsService GS = getService();
            int c = 0;
            try
            {
                DataResource.GaResource.GetRequest req = GS.Data.Ga.Get("ga:" + GATrackerId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), "ga:totalEvents,ga:uniqueEvents");
                req.Dimensions = "ga:eventAction,ga:eventLabel,ga:date,ga:hour,ga:hostname,ga:pagePath,ga:country";//,ga:eventCategory,ga:screenResolution,ga:screenColors - Not included because of not allowd more than 7

                gd = req.Execute();
                //Add rows
                int rows = gd.Rows.Count();
                for (int i = 0; i < rows; i++)
                {
                    GAEventData Ged = new GAEventData();
                    IList<String> ola = gd.Rows[i];

                    //Ged.EventCategory = ola[0].ToString();

                    Ged.EventAction = ola[0].ToString();

                    string[] EventLabel = ola[1].ToString().Split(';');
                    foreach (string item in EventLabel)
                    {
                        if (item.Substring(0, item.IndexOf(':')).ToUpper() == "ASSETID")
                        {
                            Ged.Asset_GUID = item.Substring(item.LastIndexOf(':') + 1);
                        }
                        else if (item.Substring(0, item.IndexOf(':')).ToUpper() == "ASSETNAME")
                        {
                            Ged.Asset_Name = item.Substring(item.LastIndexOf(':') + 1);
                        }
                        else if (item.Substring(0, item.IndexOf(':')).ToUpper() == "STORYID")
                        {
                            Ged.StoryId = item.Substring(item.LastIndexOf(':') + 1);
                        }
                        else if (item.Substring(0, item.IndexOf(':')).ToUpper() == "SOURCENAME")
                        {
                            Ged.Source_Name = item.Substring(item.LastIndexOf(':') + 1);
                        }
                        else if (item.Substring(0, item.IndexOf(':')).ToUpper() == "PLAYERID")
                        {
                            Ged.Player_GUID = item.Substring(item.LastIndexOf(':') + 1);
                        }
                        else if (item.Substring(0, item.IndexOf(':')).ToUpper() == "ISEMBED")
                        {
                            Ged.Is_Embed = item.Substring(item.LastIndexOf(':') + 1);
                        }
                        else if (item.Substring(0, item.IndexOf(':')).ToUpper() == "PAGEEVENT")
                        {
                            Ged.Page_Event = item.Substring(item.LastIndexOf(':') + 1);
                        }
                    }
                    Ged.Datetime = ola[2].ToString() + " " + ola[3].ToString() + ":00:00"; // Date Hour:minute:sec

                    Ged.HostName = ola[4].ToString();

                    Ged.PagePath = ola[5].ToString();

                    Ged.Country_ID = ola[6].ToString();

                    Ged.TotalEvents = Convert.ToInt64(ola[7].ToString());

                    Ged.UniqueEvents = Convert.ToInt64(ola[8].ToString());

                    eventData.Add(Ged);
                }
                return eventData;
            }
            catch (Exception ex) //TODO: should automatically get logged, will log explicitly if not 
            {
                Console.WriteLine(c + "ERROR\n" + ex.ToString());
                return null;
            }
        }
        private AnalyticsService getService()
        {
            try
            {
                // Get hardcoded values if no data is available from config
                string KeyFileLocation = System.AppDomain.CurrentDomain.BaseDirectory + @"\" + GetConfigValueOrDefault("PrivateKey", "c264884a10e73eb2817bb8734d85c699d0dcb67e-privatekey.p12");
                string keyPassword = GetConfigValueOrDefault("keyPassword", "notasecret");
                string serviceAccountEmail = GetConfigValueOrDefault("clientEmailId", "491907334380-f5mt8mspkfpmkg4uc0jlrahigdl18m8e@developer.gserviceaccount.com");
                X509Certificate2 certificate = new X509Certificate2(KeyFileLocation, keyPassword, X509KeyStorageFlags.Exportable);

                //TODO Optimize this by caching certain classes
                //TODO Also see if this can improved by using offline access via refresh-tokens etc 

                // Create credentials
                ServiceAccountCredential credential = new ServiceAccountCredential(
                   new ServiceAccountCredential.Initializer(serviceAccountEmail)
                   {
                       Scopes = new[] { AnalyticsService.Scope.AnalyticsReadonly }
                   }.FromCertificate(certificate));

                // Create the service
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "TNM GA Application",
                });
                return service;
            }
            catch (Exception)
            {
                //Console.WriteLine("GA connection object failed to initiate...\n" + ex.ToString());
                TNMLogger.Instance.Log("GA connection object failed to initiate: see exception for more details", TNMLogger.LoggerLevelSupported.Error);
                throw;
                //return null;
            }
        }
        private static string GetConfigValueOrDefault(string appSettingsKey, string defaultValue)
        {
            return ConfigurationManager.AppSettings[appSettingsKey] ?? defaultValue;
        }
    }
    public class GAEventData
    {
        public String EventCategory { get; set; }
        public String EventLable { get; set; }
        public String EventAction { get; set; }
        public string Asset_GUID { get; set; }
        public string Asset_Name { get; set; }
        public string StoryId { get; set; }
        public string Source_Name { get; set; }
        public string Player_GUID { get; set; }
        public string Is_Embed { get; set; }
        public string HostName { get; set; }
        public string PagePath { get; set; }
        public string Datetime { get; set; }
        public long TotalEvents { get; set; }
        public long UniqueEvents { get; set; }
        public string IP { get; set; }
        public string Country_ID { get; set; }
        public string Page_Event { get; set; }
    }
}
