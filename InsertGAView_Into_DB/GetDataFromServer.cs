﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;
using System.Data;

namespace InsertGAView_Into_DB
{
    public class GetDataFromServer
    {
        public string ConnectionString { get; set; }
        public DataSet outPutDataSet = new DataSet();

        public bool TestConnection()
        {
            if (ConnectionString == null)
            {
                ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
            }

            using (SqlConnection c = new SqlConnection(ConnectionString))
            {
                try
                {
                    DataTable t = new DataTable();
                    c.Open();
                    using (SqlDataAdapter a = new SqlDataAdapter("SELECT 1 AS Test", c))
                    {
                        a.Fill(t);
                    }

                    if (t.Rows.Count == 1)
                    {
                        Console.WriteLine("Connection string is working");
                        return true;
                    }

                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Connection string is not working");
                    errorLog EL = new errorLog();
                    EL.logMessage(ex.Message);
                    return false;
                }
            }
        }
        //gat the dataset from sqlServer
        public DataSet executeProc(string SqlQuery)
        {
            outPutDataSet.Clear();
            if (ConnectionString == null)
            {
                ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
            }
            errorLog EL = new errorLog();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(SqlQuery, con))
                    {
                        //EL.logMessage("Query :" + SqlQuery);
                        if (ConfigurationManager.AppSettings["CommandTimeOut"].Length > 0)
                            cmd.CommandTimeout = Convert.ToInt16(ConfigurationManager.AppSettings["CommandTimeOut"]);
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(outPutDataSet);
                            //Console.WriteLine("Query execution over");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("unable to retrive data");
                    Console.WriteLine(ex.ToString());

                    EL.logMessage("Unable to retrive data from server");
                    EL.logMessage(ex.ToString());
                }
            }
            //GetDataInfo();
            return outPutDataSet;
        }
        public DataSet executeProc(string SqlQuery, List<GAEventData> ged)
        {
            foreach (GAEventData gdt in ged)
            {
                outPutDataSet.Clear();
                if (ConnectionString == null)
                {
                    ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                }
                errorLog EL = new errorLog();
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand(SqlQuery, con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@datetime", gdt.Datetime);
                            cmd.Parameters.AddWithValue("@ip", String.IsNullOrEmpty(gdt.IP) ? "" : gdt.IP);
                            cmd.Parameters.AddWithValue("@page_event", String.IsNullOrEmpty(gdt.Page_Event) ? "0" : gdt.Page_Event);
                            cmd.Parameters.AddWithValue("@assetid", gdt.Asset_GUID);
                            cmd.Parameters.AddWithValue("@playerid", gdt.Player_GUID);
                            cmd.Parameters.AddWithValue("@action", gdt.EventAction);
                            string URL = "http://" + gdt.HostName.Replace("http://", "") + gdt.PagePath;
                            cmd.Parameters.AddWithValue("@url", URL);
                            cmd.Parameters.AddWithValue("@countryid", String.IsNullOrEmpty(gdt.Country_ID) ? "" : gdt.Country_ID);
                            cmd.Parameters.AddWithValue("@isembed", gdt.Is_Embed);
                            cmd.Parameters.AddWithValue("@clientname", gdt.Source_Name);
                            cmd.Parameters.AddWithValue("@domainname", gdt.HostName);
                            if (string.IsNullOrEmpty(gdt.StoryId))
                            {
                                cmd.Parameters.AddWithValue("@StoryID", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@StoryID", gdt.StoryId);
                            }
                            cmd.Parameters.AddWithValue("@totalevents", gdt.TotalEvents);
                            cmd.Parameters.AddWithValue("@uniqueevents", gdt.UniqueEvents);

                            if (ConfigurationManager.AppSettings["CommandTimeOut"].Length > 0)
                                cmd.CommandTimeout = Convert.ToInt16(ConfigurationManager.AppSettings["CommandTimeOut"]);
                            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                            {
                                sda.Fill(outPutDataSet);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("unable to retrive data");
                        Console.WriteLine(ex.ToString());

                        EL.logMessage("Unable to retrive data from server");
                        EL.logMessage(ex.ToString());
                    }
                }
                //GetDataInfo();
            }
            return outPutDataSet;
        }

        public void GetDataInfo(DataSet DS)
        {
            if (DS != null)
            {
                Console.WriteLine("\n\nDataset info");
                Console.WriteLine("Dataset has " + outPutDataSet.Tables.Count + " data tables\n");

                Console.WriteLine(String.Format("{0,-35} | {1,-10} | {2,-10} |", "TableName", "Rows", "Columns"));
                Console.WriteLine(String.Format("{0,-35} | {1,-10} | {2,-10} |", "-----------------------------------", "----------", "----------"));
                foreach (DataTable DT in DS.Tables)
                {
                    //Console.WriteLine("Data table **" + DT.TableName + "** has (" + DT.Rows.Count + " Rows X " + DT.Columns.Count + " Columns )");
                    Console.WriteLine(String.Format("{0,-35} | {1,-10} | {2,-10} |", DT.TableName, DT.Rows.Count, DT.Columns.Count));
                }
                Console.WriteLine(String.Format("{0,-35} | {1,-10} | {2,-10} |", "-----------------------------------", "----------", "----------"));
            }
            else
            {
                Console.WriteLine("There is no data retrieved from the DB");
            }
        }
    }
}
