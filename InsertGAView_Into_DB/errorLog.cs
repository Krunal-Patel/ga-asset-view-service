﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertGAView_Into_DB
{
    class errorLog
    {
        public void logMessage(string Message)
        {
            try
            {
                if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\Logs\\"))
                {
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\Logs\\");
                }
                DateTime DT = DateTime.Now;
                string Roller = DT.Day + "_" + DT.Month + "_" + DT.Year;
                TextWriter tw = new StreamWriter(Directory.GetCurrentDirectory() + "\\Logs\\" + Roller + "_Log.txt", true);
                //Console.WriteLine(Directory.GetCurrentDirectory());
                tw.Write(DateTime.Now.ToString() + " :: ");
                tw.Write(Message + "\n");
                tw.Close();
            }
            catch (Exception EX)
            {
                Console.WriteLine("Error Writing to log file");
                Console.Write(EX.ToString());
            }
        }
    }
}
