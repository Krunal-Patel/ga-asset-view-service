﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Xml.XPath;
using System.Data;
using System.Globalization;
using TNM.GAData;

namespace InsertGAView_Into_DB
{
    public class Program
    {
        static errorLog el = new errorLog();
        static void Main(string[] args)
        {
            int ProfileID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["GA_AccountID"]);
            try
            {
                DateTime DT = DateTime.Now;
                int startDay = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Start_Date"]);
                int endDay = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["End_Date"]);
                if (startDay > 0)
                {
                    startDay = startDay * -1;
                }
                if (endDay > 0)
                {
                    endDay = endDay * 1;
                }
                DateTime sDay = DT.AddDays(startDay);
                DateTime eDay = DT.AddDays(endDay);

                GADataFetcher GDF = new GADataFetcher();
                List<GAEventData> ged = new List<GAEventData>();
                GetDataFromServer GDFS = new GetDataFromServer();
                ged = GDF.Get_GAEventsData(ProfileID, sDay, eDay);

                string strMessage = string.Empty;
                GDFS.executeProc("tnm_ga_data_post_process", ged);
            }
            catch (Exception ex)
            {
                el.logMessage(ex.ToString());
            }
        }
    }
}
