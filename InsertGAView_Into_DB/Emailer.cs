﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace InsertGAView_Into_DB
{
    class Emailer
    {
        public string SendTheMail(string subject, string reciPients_TO, string reciPients_CC, string HtmlBody)
        {
            errorLog el = new errorLog();
            try
            {
                SmtpClient client = new SmtpClient();

                client.Host = System.Configuration.ConfigurationManager.AppSettings["SMTPHost"];

                MailMessage message = new MailMessage();
                message.IsBodyHtml = true;
                try
                {
                    message.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailFrom"], System.Configuration.ConfigurationManager.AppSettings["MailFromName"]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                //add Users
                string[] ToUsers = reciPients_TO.Split(',');
                if (ToUsers != null && reciPients_TO != "")
                {
                    foreach (string email in ToUsers)
                    {
                        message.To.Add(email);
                    }
                }

                //add CC Users
                string[] CCUsers = reciPients_CC.Split(',');
                if (CCUsers != null && reciPients_CC != "")
                {
                    foreach (string email in CCUsers)
                    {
                        message.CC.Add(email);
                    }
                }

                //add BCC Users
                string reciPients_BCC = System.Configuration.ConfigurationManager.AppSettings["MailBCC"];
                string[] BCCUsers = reciPients_BCC.Split(',');
                if (BCCUsers != null && reciPients_BCC != "")
                {
                    foreach (string email in BCCUsers)
                    {
                        message.Bcc.Add(email);
                    }
                }

                //mail text 
                message.Body = HtmlBody;
                //Subject
                message.Subject = subject;

                if (reciPients_TO.Length > 1 && reciPients_TO != null && HtmlBody.Length > 10)
                {
                    client.Send(message);
                    el.logMessage("Email sent out");
                }
                Console.WriteLine("Email sent out");
                client.Dispose();
                return "Mail sent out";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                el.logMessage("Unable to send the mail out" + ex.ToString());
                return "Unable to send the mail out";
            }
        }
    }
}
