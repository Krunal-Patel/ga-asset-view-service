﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.IO;
using GaViewWebAPI.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Text;

namespace GaViewWebAPI.Controllers
{
    public class RequestController : ApiController
    {
        [HttpGet]
        public void GetRequestinfo([FromUri] GaRequestMetricParam garequestparam)
        {
            string EVENTTYPE = garequestparam.events;
            string EVENTCATEGORY = null;

            if (EVENTTYPE == "Image Load" || EVENTTYPE == "Image Preview")
            {
                EVENTCATEGORY = "Stills";
            }
            else if (EVENTTYPE == "Video Stream Load" || EVENTTYPE == "Video Stream Start")
            {
                EVENTCATEGORY = "Video";
            }
            else if (EVENTTYPE == "Audio Load" || EVENTTYPE == "Audio Start")
            {
                EVENTCATEGORY = "Audio";
            }
            int PAGEEVENT = 32;
            if (EVENTTYPE == "Image Load" || EVENTTYPE == "Video Stream Load" || EVENTTYPE == "Audio Load")
            {
                PAGEEVENT = 10;
            }
            else if (EVENTTYPE == "Image Preview" || EVENTTYPE == "Video Stream Start" || EVENTTYPE == "Audio Start")
            {
                PAGEEVENT = 31;
            }

            string EVENTLABLE = String.Format("AssetId:{0};AssetName:{1};StoryId:{2};SourceName:{3};PlayerId:{4};IsEmbed:{5};PageEvent:{6}", garequestparam.c1, garequestparam.c2, garequestparam.c7, garequestparam.v20, garequestparam.c4, garequestparam.v17, PAGEEVENT);

            string SCREEN_RESOLUTION = garequestparam.s;
            string SCREEN_COLORDEPTH = garequestparam.c + "-bit";
            string HOSTNAME = String.IsNullOrEmpty(garequestparam.r) ? garequestparam.g : garequestparam.r;
            string PAGETITLE = garequestparam.pagename;
            string PAGE = null;
            if (!String.IsNullOrEmpty(garequestparam.r))
            {
                PAGE = garequestparam.g.Replace(garequestparam.r, "");
            }
            else
            {
                PAGE = garequestparam.g;
            }
            GoogleAnalyticsApi.TrackEvent(EVENTCATEGORY, EVENTTYPE, EVENTLABLE, SCREEN_RESOLUTION, SCREEN_COLORDEPTH, HOSTNAME, PAGETITLE, PAGE, 55);
        }
        private Random _random;
        private string GenerateRandomId()
        {
            _random = new Random();
            string randomId = "";

            for (int i = 0; i < 10; i++)
            {
                randomId += _random.Next(9).ToString();
            }

            return randomId;
        }
    }
}
