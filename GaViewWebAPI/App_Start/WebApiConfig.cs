﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace GaViewWebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.IgnoreRoute("Specific", "{resource}.axd/{*pathInfo}");
            // Web API Attribute routing
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new
                {
                    action = RouteParameter.Optional,
                    id = RouteParameter.Optional
                }
            );
            config.Routes.MapHttpRoute(
                name: "RequestApi",
                routeTemplate: "api/{b}/{ss}/{newsmarketstreamingdev}/{1}/{H.25.5}/{s37663459515465}/{controller}/{action}",
                defaults: new
                {
                    action = RouteParameter.Optional,
                    id = RouteParameter.Optional
                }
            );
        }
    }
}
