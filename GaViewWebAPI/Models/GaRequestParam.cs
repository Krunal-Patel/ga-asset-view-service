﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GaViewWebAPI.Models
{
    //public class GaRequestParam
    //{
    //    public string DateTime { get; set; } //Datetime
    //    public string fid { get; set; } //VisitorID
    //    public string pageName { get; set; } //pageName
    //    public string HostName { get; set; } //HostName
    //    public string Page { get; set; } //URL
    //    public string ScreenResolution { get; set; } //Screen Resolution
    //    public string ScreenColor { get; set; } //Screen Color
    //    public string bw { get; set; } //Browser Width
    //    public string bh { get; set; } //Browser Height
    //    public string EventType { get; set; } //EventType
    //    public string EventCategory { get; set; } //EventCategory
    //    public string AssetID { get; set; } //Asset_ID
    //    public string AssetName { get; set; } //Asset_Name    
    //    public string Player_ID { get; set; } //Player_ID
    //    public string SourceName { get; set; } //Client Name
    //    public string StoryID { get; set; } //Story_ID
    //    public string IsEmbeds { get; set; } //IsEmbeds
    //    public string ParentPageURL { get; set; } //ParentPageURL

    //}
    public class GaRequest
    {
        //public GaRequestParam GaRequestParam { get; set; }
        public GaRequestMetricParam GaRequestMetricParam { get; set; }
    }
    public class GaRequestMetricParam
    {
        private string _AQB;
        public string AQB { get { return _AQB; } set { _AQB = value; } }

        private string _ndh;
        public string ndh { get { return _ndh; } set { _ndh = value; } }

        private string _t { get; set; }
        public string t { get { return _t; } set { _t = value; } }

        private string _fid { get; set; }
        public string fid { get { return _fid; } set { _fid = value; } }

        private string _ce { get; set; }
        public string ce { get { return _ce; } set { _ce = value; } }

        private string _ns { get; set; }
        public string ns { get { return _ns; } set { _ns = value; } }

        private string _pagename { get; set; }
        public string pagename { get { return _pagename; } set { _pagename = value; } }

        private string _g { get; set; }
        public string g { get { return _g; } set { _g = value; } }

        private string _r { get; set; }
        public string r { get { return _r; } set { _r = value; } }

        private string _cc { get; set; }
        public string cc { get { return _cc; } set { _cc = value; } }

        private string _events { get; set; }
        public string events { get { return _events; } set { _events = value; } }

        private string _c1 { get; set; }
        public string c1 { get { return _c1; } set { _c1 = value; } }

        private string _c2 { get; set; }
        public string c2 { get { return _c2; } set { _c2 = value; } }

        private string _c4 { get; set; }
        public string c4 { get { return _c4; } set { _c4 = value; } }

        private string _c5 { get; set; }
        public string c5 { get { return _c5; } set { _c5 = value; } }

        private string _c7 { get; set; }
        public string c7 { get { return _c7; } set { _c7 = value; } }

        private string _v11 { get; set; }
        public string v11 { get { return _v11; } set { _v11 = value; } }

        private string _v12 { get; set; }
        public string v12 { get { return _v12; } set { _v12 = value; } }

        private string _v14 { get; set; }
        public string v14 { get { return _v14; } set { _v14 = value; } }

        private string _v15 { get; set; }
        public string v15 { get { return _v15; } set { _v15 = value; } }

        private string _v16 { get; set; }
        public string v16 { get { return _v16; } set { _v16 = value; } }

        private string _v17 { get; set; }
        public string v17 { get { return _v17; } set { _v17 = value; } }

        private string _v18 { get; set; }
        public string v18 { get { return _v18; } set { _v18 = value; } }

        private string _v20 { get; set; }
        public string v20 { get { return _v20; } set { _v20 = value; } }

        private string _pe { get; set; }
        public string pe { get { return _pe; } set { _pe = value; } }

        private string _pev2 { get; set; }
        public string pev2 { get { return _pev2; } set { _pev2 = value; } }

        private string _s { get; set; }
        public string s { get { return _s; } set { _s = value; } }

        private string _c { get; set; }
        public string c { get { return _c; } set { _c = value; } }

        private string _j { get; set; }
        public string j { get { return _j; } set { _j = value; } }

        private string _v { get; set; }
        public string v { get { return _v; } set { _v = value; } }

        private string _k { get; set; }
        public string k { get { return _k; } set { _k = value; } }

        private string _bw { get; set; }
        public string bw { get { return _bw; } set { _bw = value; } }

        private string _bh { get; set; }
        public string bh { get { return _bh; } set { _bh = value; } }

        private string _p { get; set; }
        public string p { get { return _p; } set { _p = value; } }

        private string _AQE { get; set; }
        public string AQE { get { return _AQE; } set { _AQE = value; } }
    }
}